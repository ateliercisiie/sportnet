# Spornet

## Projet

Le projet est une application permettant de gérer des événements sportifs et offre aux athlètes la possibilité de s'inscrire aux différentes épreuves proposées.

## Équipe

L'équipe est constituée de : Dylan Demougin, Maxime Weiten, David Lambiase, Quentin Claudel

## Installation

Pour installer le projet, il est nécessaire de modifier le fichier *config/settings.php* pour y intégrer les bons identifiants de base de données.
Il faut ensuite placer le DocumentRoot sur le dossier *public/*. L'application est ensuite fonctionnelle.

## Fonctionnalités 

Les fonctionnalités sont les suivantes: 

- inscription/connexion en tant qu'organisateur

- création/suppression d'événement (et ajout de photo)

- création/suppression d'épreuve

- participation à une épreuve

- téléchargement du fichier des participants à une épreuve (et numéro de dossard) en .csv

- upload de résultats en .csv

- recherche du résultat personnel d'un utilisateur à partir de son numéro de participant

- modification du profil d'un organisateur (et ajout d'une photo de profil)