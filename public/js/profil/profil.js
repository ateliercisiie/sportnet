$(function () {
    moreEvent();

    function moreEvent() {
        $(document).on('click', '#more', function () {
            var id_event = $('.blocEvent').last().attr("data-id");
            var id_user = $('.profilData').attr("data-id");

            $.ajax({
                type: 'GET',
                url: "/events/" + id_event + "/user/" + id_user + "/more/",
                success: function (data) {

                    if (jQuery.isEmptyObject(data)) {
                        $('#more').val('Plus dévénements disponible');
                        $('#more').attr('disabled', 'disabled')
                    } else {
                        $.each(data, function (k, event) {
                            var html = '<div class="blocEvent" data-id="' + event.id + '">'
                                + '<h4><a class="titleEvent" href="/events/display/'+ event.id+'">'+ event.name + '</a></h4>'
                                + '<span class="date_event">' + event.date + '</span>'
                                + '<a href="/events/display/' + event.id + '"><img src="/images/events/' + event.id + '.jpg" alt=""></a>'
                                + '<ul class="event_info">'
                                + '<li class="organisateur">Organisateur:'
                                + '<a href="/user/' + event.parent_owner.id + '">'
                                + event.parent_owner.first_name + ' ' + event.parent_owner.last_name
                                + '</a>'
                                + '</li>'
                                + '<li class="location">' + 'Lieu:' + event.location + '</li>'
                                + '<li class="sport">'+'Discipline:' + event.sport.name + '</li> '
                                + '<li class="description">Description:'
                                + event.description
                                + '</li>'
                                +' <li class="details">'
                                +'<a  href="/events/display/'+ event.id +'">'
                                +'<input type="button" value="Détails" class="detailButton"/>'
                                +'</a></li>'
                                +'<li class="supprimer"><a  href="/events/remove/'+ event.id +'"><input type="button" value="Supprimer" class="btn red"/></a></li>'
                                + '</ul>'
                                + '</div>';


                            html = $(html).hide();
                            $('.blocListEvents').append(html);
                            html.slideDown(500);
                        });
                    }

                }
            })
        });
    }


});