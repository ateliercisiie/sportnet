function changeTab() {
    $('.tab li').on('click', function () {
        if ($(this).attr('class') !== 'active') {
            $('.active').removeClass('active');
            $(this).addClass('active');

            if ($(this).attr('id') === 'tab-1') {
                $('#tab-content-2').addClass('hidden-small hidden-medium hidden-large');
                $('#tab-content-1').removeClass('hidden-small hidden-medium hidden-large');
            } else {
                $('#tab-content-1').addClass('hidden-small hidden-medium hidden-large');
                $('#tab-content-2').removeClass('hidden-small hidden-medium hidden-large');
            }
        }
    });
}


