$(function () {


    addVisitor();
    resetForm();

    /*
     * function pour des participants simultanément
     * Il crée un formulaire avec champs crée selon le nombre de particpants
     * le name de chaque chammps change en fonction du nb de participant
     * Ce qui permet de les récupérer en php simultanément
     */
    function addVisitor() {

        $(document).on('click', '#add_visitor_btn', function (event) {
            event.preventDefault();
            var nbParticipant = $('#nb_participant').val();
            var nbForm = parseInt(nbParticipant);
            var selectorForm = $('#form_visitor');

            $('#add_visitor_btn').attr("disabled", "disabled");
            $('#add_visitor_btn').css('cursor', " default");

            for (var i = 1; i <= nbForm; i++) {
                var html = formVisitor(i);
                if (i === 1) {
                    selectorForm.append(html);
                } else {
                    var form = $(html).hide();
                    selectorForm.append(form);

                }
            }

            var btn_next = '<div class="input-lg " id="next_container">'
                + '<input id="next" class="btn green" value="Ajouter ce participant"/>'
                + '</div>';

            selectorForm.append(btn_next);
            var current_form_index = 1;

            $.dobPicker({
                daySelector: '.dobday_'+current_form_index, /* Required */
                monthSelector: '.dobmonth_'+current_form_index, /* Required */
                yearSelector: '.dobyear_'+current_form_index, /* Required */
                dayDefault: 'Day', /* Optional */
                monthDefault: 'Month', /* Optional */
                yearDefault: 'Year', /* Optional */
                minimumAge: 12, /* Optional */
                maximumAge: 80 /* Optional */
            });

            $('#nb_participant_tittle').append(nbParticipant);
            $('.tittle').show();

            $('#next').click({param1: current_form_index, param2: nbParticipant}, function () {
                console.log(current_form_index + ' ' + nbParticipant);


                if (current_form_index == nbParticipant) {

                    $('input[name="lastname_' + current_form_index + '"]').parent().hide();
                    $('input[name="firstname_' + current_form_index + '"]').parent().hide();
                    $('select[name="jour_' + current_form_index + '"]').parent().hide();
                    $('select[name="mois_' + current_form_index + '"]').parent().hide();
                    $('select[name="annee_' + current_form_index + '"]').parent().hide();
                    $('#next').attr("disabled", "disabled");
                    $('#form_visitor').append('<input type="submit" class="btn green raised" value="Réserver">');



                } else {
                    $('input[name="lastname_' + current_form_index + '"]').parent().hide().fadeOut(500);
                    $('input[name="firstname_' + current_form_index + '"]').parent().hide().fadeOut(500);
                    $('select[name="jour_' + current_form_index + '"]').parent().hide().fadeOut(500);
                    $('select[name="mois_' + current_form_index + '"]').parent().hide().fadeOut(500);
                    $('select[name="annee_' + current_form_index + '"]').parent().fadeOut(500);

                    current_form_index++;
                    $('input[name="lastname_' + current_form_index + '"]').parent().fadeIn(500);
                    $('input[name="firstname_' + current_form_index + '"]').parent().fadeIn(500);
                    $('select[name="jour_' + current_form_index + '"]').parent().fadeIn(500);
                    $('select[name="mois_' + current_form_index + '"]').parent().hide().fadeIn(500);
                    $('select[name="annee_' + current_form_index + '"]').parent().fadeIn(500);

                    $.dobPicker({
                        daySelector: '.dobday_'+current_form_index, /* Required */
                        monthSelector: '.dobmonth_'+current_form_index, /* Required */
                        yearSelector: '.dobyear_'+current_form_index, /* Required */
                        dayDefault: 'Day', /* Optional */
                        monthDefault: 'Month', /* Optional */
                        yearDefault: 'Year', /* Optional */
                        minimumAge: 12, /* Optional */
                        maximumAge: 80 /* Optional */
                    });


                    $('#current_nb_participant_tittle').empty();
                    $('#current_nb_participant_tittle').append(current_form_index);

                }
            });



        });

    }

    /*
     * Crée les champs nécessaires en html dans un variable
     */
    function formVisitor(i) {
        var html = '';

        //input name
        html += '<div class="input-lg">'
            + '<label for="lastname"> Nom</label>'
            + '<input type="text" id="lastname" name="lastname_' + i + '" value="" placeholder="Nom">'
            + '</div>';

        //input first name
        html += '<div class="input-lg">'
            + '<label for="firstname"> Prénom</label>'
            + '<input type="text" id="firstname" name="firstname_' + i + '" value="" placeholder="Prénom">'
            + '</div>';

        // input birthday
        html += '<div class="input-lg">'
            + '<label for="birthDate">Date de Naissance</label>'
            + '<select name="jour_' + i + '" class="dobday_'+ i + '"></select>'
            + '<select name="mois_' + i + '" class="dobmonth_'+ i + '"></select>'
            + '<select name="annee_' + i + '" class="dobyear_'+ i + '"></select>'
            + '</div>';

        return html

    }

    /*
     * Reset form if the users change the value of the select
     */
    function resetForm() {
        $('#nb_participant').on('change', function () {
            $('#form_visitor').empty();
            $('#add_visitor_btn').removeAttr('disabled');
        });
    }

});
