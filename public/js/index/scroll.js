$(function () {
    moreEvent();
    moreEventSport();
    fixedMenuLeft();

    function createDate($date){
        var dateFr = $date;
        var dd = dateFr.substr(8,2);
        var mm = dateFr.substr(5,2);
        var yy = dateFr.substr(0,4);
        return dd+'/'+mm+'/'+yy;
    }

    function moreEvent() {
        $(document).on('click', '#more', function () {
            var id_event = $('.event').last().attr("data-id");

            $.ajax({
                type: 'GET',
                url: "/events/more/" + id_event,
                success: function (data) {
                    if (jQuery.isEmptyObject(data)) {
                        $('#more').val('Plus dévénements disponible');
                        $('#more').attr('disabled', 'disabled')
                    } else {
                        $.each(data, function (k, event) {
                            var html = '<div class="event" data-id="' + event.id + '">'
                                + '<h4><a class="titleEvent" href="/events/display/' + event.id + '">'
                                + event.name
                                + '</a></h4>'
                                + '<span class="date_event">' + createDate(event.date) + '</span>'
                                + '<a href="/events/display/' + event.id + '"><img src="/images/events/'+ event.id +'.jpg" alt="Image événement"></a>'
                                + '<ul class="event_info">'
                                + ' <li class="organisateur">Organisateur:'
                                + '<a href="/profile/'+event.parent_owner.id+'">'
                                + event.parent_owner.first_name + ' ' + event.parent_owner.last_name + '</a>'
                                + '</li>'
                                + '  <li class="location">Lieu:' + event.location + '</li>'
                                + '<li>Discipline: ' + event.sport.name + '</li>'
                                + '<li class="description">Description:' + event.description + '</li>'
                                + '</ul>'
                                + '</div>';

                            html = $(html).hide();
                            $('.colum_event').append(html);
                            html.slideDown(500);
                        });
                    }

                }
            })
        });
    }


    function moreEventSport() {
        $(document).on('click', '#moreSport', function () {
            var id_event = $('.event').last().attr("data-id");
            var id_sport = $('.event').last().attr("sport-id");

            $.ajax({
                type: 'GET',
                url: "/events/moreSport/" + id_event + "/" +id_sport,
                success: function (data) {
                    if (jQuery.isEmptyObject(data)) {
                        $('#moreSport').val('Plus dévénements disponible');
                        $('#moreSport').attr('disabled', 'disabled')
                    } else {
                        $.each(data, function (k, event) {
                            var html = '<div class="event" data-id="' + event.id + '" sport-id="'+ id_sport +'">'
                                + '<h4><a class="titleEvent" href="/events/display/' + event.id + '">'
                                + event.name
                                + '</a></h4>'
                                + '<span class="date_event">' + createDate(event.date) + '</span>'
                                + '<a href="/events/display/' + event.id + '"><img src="/images/events/'+ event.id +'.jpg" alt="Image Evenement"></a>'
                                + '<ul class="event_info">'
                                + ' <li class="organisateur">Organisateur:'
                                + '<a href="/profile/'+event.parent_owner.id+'">'
                                + event.parent_owner.first_name + ' ' + event.parent_owner.last_name + '</a>'
                                + '</li>'
                                + '  <li class="location">Lieu:' + event.location + '</li>'
                                + '<li>Discipline: ' + event.sport.name + '</li>'
                                + '<li class="description">Description:' + event.description + '</li>'
                                + '</ul>'
                                + '</div>';

                            html = $(html).hide();
                            $('.colum_event').append(html);
                            html.slideDown(500);
                        });
                    }

                }
            })
        });
    }

    function fixedMenuLeft() {
        var menu_selector = $("menu");
        var pos = menu_selector.offset().top;

        $(window).scroll(function () {
            var currentPos = $(document).scrollTop();

            if (currentPos >= pos) {
                if ($(window).width() > 775) {
                    menu_selector.css('position', 'relative');
                    menu_selector.css('top', currentPos + 'px');
                    menu_selector.css('margin-top', -10+ 'px')
                }
            } else {
                menu_selector.css('position', 'static');
                menu_selector.css('top', '');
                menu_selector.css('margin-top', '');
            }

        });
    }

});