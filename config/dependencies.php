<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../templates/', [
        'debug' => true,
        'cache' => false,
    ]);
    $container['view'] = function ($container) {
        return new \Slim\Views\PhpRenderer('path/to/templates/with/trailing/slash/');
    };

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));
    // This line should allow the use of {{ dump() }}
    $view->addExtension(new \Twig_Extension_Debug());
    return $view;
};


// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Service factory for the ORM
$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};


// Register provider
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};


$container['sentinel'] = function () {
    return ((new \Cartalyst\Sentinel\Native\Facades\Sentinel())->getSentinel());
};
