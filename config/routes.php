<?php
// Routes

$homeController = new App\Controllers\HomeController($app);
$userController = new \App\Controllers\UserController($app);
$eventsController = new \App\Controllers\EventsController($app);
$sessionsController = new \App\Controllers\SessionsController($app);
$visitorController = new \App\Controllers\VisitorsController($app);

$app->group('', function () use ($app, $homeController) {
    $app->get('/', $homeController('accueil'));
    $app->get('/sport/{id}', $homeController('displaySport'));
});

$app->group('/profil', function () use ($app, $userController) {
    $app->group('/', function () use ($app, $userController) {
        $app->get('me', $userController('profileView'));
        $app->get('update', $userController('profileModifView'));
        $app->post('update', $userController('profileUpdate'));
    })->add(new \App\middleware\AuthMiddleware($app));

    $app->get('/{id}', $userController('UserView'));
});

$app->group('/user', function () use ($app, $userController) {
    $app->post('/join/{id}', $userController('join'));;
});


$app->group('/auth', function () use ($app, $userController) {
    $app->get('/register', $userController('registerView'));
    $app->get('/login', $userController('loginView'));
    $app->post('/register', $userController('register'));
    $app->post('/login', $userController('login'));
    $app->get('/logout', $userController('logout'));
    $app->get('/activate/{code}/{login}', $userController('confirm_account'));
    $app->get('/reminder', $userController('reminderPwd'));
    $app->post('/reminder', $userController('reminderPost'));
    $app->get('/reminder/{token}/{user_id}', $userController('reminderForm'));
    $app->post('/reminder/{token}/{user_id}',
        $userController('reminderChangePwd'));
});

$app->group('/events', function () use ($app, $eventsController) {
    $app->get('/display/{eventId}', $eventsController('displayEvent'));
    $app->get('/close/{id}', $eventsController('closeInscription'));
    $app->get('/open/{id}', $eventsController('openInscription'));

    $app->group('/', function () use ($app, $eventsController) {
        $app->get('create', $eventsController('createEventView'));
        $app->post('add', $eventsController('addEvent'));
        $app->post('edit/{id}', $eventsController('edit'));
        $app->get('remove/{id}', $eventsController('removeEvent'));
        $app->delete('destroy/{id}', $eventsController('destroy'));
        $app->get('/public/{id}', $eventsController('updatePublication'));
    })->add(new \App\middleware\AuthMiddleware($app));

    //api pour le scroll en ajax (voir plus)
    $app->get('/{id_event}/user/{id_user}/more/', $eventsController('getMoreByUser'))->add(new \App\middleware\RestMiddleware($app));
    $app->get('/more/{id}', $eventsController('getMore'))->add(new \App\middleware\RestMiddleware($app));
    $app->get('/moreSport/{idEvent}/{idSport}', $eventsController('getMoreSport'))->add(new \App\middleware\RestMiddleware($app));
});

$app->group('/sessions', function () use ($app, $sessionsController) {

    $app->group('/', function () use ($sessionsController, $app) {
        $app->get('create/{eventId}', $sessionsController('createSessionView'));
        $app->post('add', $sessionsController('addSession'));
        $app->post('edit/{id}', $sessionsController('edit'));
        $app->get('remove/{id}', $sessionsController('removeSession'));
        $app->post('upload/{id}', $sessionsController('uploadResults'));
        $app->get('results/hidden/{id}', $sessionsController('hideResults'));
    })->add(new \App\middleware\AuthMiddleware($app));

    $app->get('/display/{sessionId}', $sessionsController('displaySession'));
    $app->delete('/destroy/{id}', $sessionsController('destroy'));
    $app->get('/participants/{sessionId}', $sessionsController('getParticipantsFile'));
});


$app->group('/visitors', function () use ($app, $visitorController) {
    $app->post('/join/{id}', $visitorController('join'));
    $app->get('/join/{id}', $visitorController('createJoinView'));
    $app->get('/pay/{id}', $visitorController('pay'));
    $app->post('/pay/{id}', $visitorController('payPost'));
});

