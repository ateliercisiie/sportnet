<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 15/11/2016
 * Time: 22:17
 */

namespace App\Controllers;


use App\Model\Events;
use App\Model\Sports;
use App\Utils\UploadUtility;
use Cartalyst\Sentinel\Activations\IlluminateActivationRepository;
use Cartalyst\Sentinel\Checkpoints\ThrottleCheckpoint;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Reminders\EloquentReminder;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Support\Facades\Event;
use MartynBiz\Slim3Controller\Controller;
use Cartalyst\Sentinel\Checkpoints\CheckpointInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Ramsey\Uuid\Uuid;
use App\Model\Sessions;
use App\Model\Visitors;
use App\Model\Participations;
use Illuminate\Database\Eloquent;


class UserController extends Controller
{
    const PREFIX_SALT = 'sport';
    const SUFFIX_SALT = 'net';

    /**
     * Methode retournant la vue du formulaire de connexion
     */
    public function loginView()
    {
        return $this->render('auth/login.html');
    }

    /**
     * Methode retournant la vue du profil
     */
    public function profileView()
    {
        $this->UserView(Sentinel::getUser()->id);
    }


    public function UserView($id)
    {
        $profil = EloquentUser::where('id', '=', $id)->first();
        $owner = false;

        if (!$profil) {
            $this->get('flash')->addMessage('error', 'utilisateur introuvable');
            return $this->redirect('/');
        }

        $events = Events::where('user_id', $id)->where('validated', true)->orderBy("created_at", 'desc')->take(4)->get(array('id', 'name', 'location', 'registrable', 'created_at', 'sport_id'));
        $nbEvents = Events::where('user_id', $profil->id)->count();
        $nbEventsDispo = Events::where('user_id', $profil->id)->where('validated', 1)->count();
        if ($user = Sentinel::check()) {
            if ($id == $user->id)
                $owner = true;
        }

        $sports = array();
        foreach ($events as $event) {
            $sports[$event->id] = $event->sport->name;
        }

        $datas = array('user' => $user, 'profil' => $profil, 'events' => $events, 'sports' => $sports, 'owner' => $owner, 'nbEvents' => $nbEvents, 'nbEventsDispo' => $nbEventsDispo);
        return $this->render('user/profil.html', $datas);
    }

    /**
     * Methode retournant la vue de modification du profil
     */
    public function profileModifView()
    {
        $datas = array('user' => $this->get('sentinel')->getUser());
        return $this->render('user/profilModif.html', $datas);
    }

    /**
     * Methode gerant l'update d'un profil
     */
    public function profileUpdate()
    {
        $data = $this->getPost();
        $file = $_FILES;
        $modif = false;         //Si le profil a subit des modifications
        $user = $this->get('sentinel')->getUser();

        $oldPassword = filter_var($data['oldPassword'], FILTER_SANITIZE_STRING);
        $newPassword = filter_var($data['newPassword'], FILTER_SANITIZE_STRING);
        $newPasswordVerif = filter_var($data['newPasswordVerif'], FILTER_SANITIZE_STRING);
        $desc = filter_var($data['description'], FILTER_SANITIZE_STRING);
        $first_name = filter_var($data['first_name'], FILTER_SANITIZE_STRING);
        $last_name = filter_var($data['last_name'], FILTER_SANITIZE_STRING);

        //Si il a changer la photo de profil
        if (!empty($file['pictureInput']['name'])) {
            $path = 'images/profils/';
            $fileUpload = new UploadUtility($path, 'pictureInput', ['nameFile' => $user->id]);
            $success = $fileUpload->upload();

            if (!$success['success']) {
                $this->get('flash')->addMessage('error', $success['error'][0]);
                return $this->redirect('/profil/update');
            } else {
                if ($user->profilePicturePath == 'default.png') {
                    $user->profilePicturePath = $success['name'];

                }
                $modif = true;
            }
        }

        //Si l'utilisateur à changé de description
        if ($desc != $user->description) {
            $user->description = $desc;
            $modif = true;
        }

        //si l'utilisateur a changé son mot de passe
        if (!empty($newPassword) && !empty($newPasswordVerif)) {
            //verification si les nouveaux mot de passe sont les meme
            if ($newPassword == $newPasswordVerif) {
                //verification si l'ancien mot de passe est entré et si il est correct
                if (!empty($oldPassword)) {
                    $oldPassword = sha1(self::PREFIX_SALT . $oldPassword . self::SUFFIX_SALT);
                    $hasher = Sentinel::getHasher();
                    //regarde si l'ancien mot de passe correspond bien au mot de passe de la base
                    if ($hasher->check($oldPassword, $user->password)) {
                        $newPassword = sha1(self::PREFIX_SALT . $newPassword . self::SUFFIX_SALT);
                        $credentials = [
                            'password' => $newPassword
                        ];
                        if (Sentinel::update($user, $credentials)) {
                            $modif = true;
                        } else {
                            $this->get('flash')->addMessage('error', 'Problème lors du changement de mot de passe');
                        }
                    } else {
                        $this->get('flash')->addMessage('error', 'Mot de passe incorrect');
                    }
                }
            } else {
                //si les mots de passe ne sont pas les meme
                $this->get('flash')->addMessage('error', 'Les mots de passes ne correspondent pas.');
            }
        }


        //si l'utilisateur a change de nom
        if (!empty($last_name)) {
            $user->last_name = $last_name;
            $modif = true;
        }

        //si l'utilisateur a change de prenom
        if (!empty($last_name)) {
            $user->first_name = $first_name;
            $modif = true;
        }


        //si il y a eu des modifications dans le profil
        if ($modif) {
            $user->update();
            $this->get('flash')->addMessage('success', 'Votre profil à bien été modifié');
        }

        return $this->redirect('/profil/me');
    }

    /**
     * Methode retournant la vue du formulaire d'inscription
     */
    public function registerView()
    {
        return $this->render('auth/register.html');
    }

    /**
     * Methode permettant d'enregistrer un utilisateur a partir du formulaire
     */
    public function register()
    {
        $data = $this->getPost();

        if (empty($data['email']) || empty($data['password'])) {
            $this->get('flash')->addMessage('error', 'Le champ mot de passe et email sont requis.');
            return $this->redirect('/auth/register');
        }

        if ($data['password'] != $data['confirm_password']) {
            $this->get('flash')->addMessage('error', 'Le mot passe et la confirmation du mot de passe ne correspondent pas.');
            return $this->redirect('/auth/register');
        }

        //$role = $this->get("sentinel")->findRoleByName('Admin');
        if ($this->get('sentinel')->findByCredentials([
            'login' => $data['email'],
        ])
        ) {
            $this->get('flash')->addMessage('error', 'Cette adresse email correspond déja à un compte.');
            return $this->redirect("/auth/register");
        }

        if (empty($data['annee']) || empty($data['mois'] || empty($data['jour']))) {
            $date = new \DateTime($data['annee'] . '-' . $data['mois'] . '-' . $data['jour']);
            $stringDate = $date->format('Y-m-d');
        }

        // create salt for pwd
        $pwd = sha1(self::PREFIX_SALT . $data['password'] . self::SUFFIX_SALT);
        $id = Uuid::uuid4();

        $date = new \DateTime($data['birthday']);
        $birthday = $date->format('Y-m-d H:i:s');

        $user = $this->get("sentinel")->create([
            'id' => $id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'birthdate' => $birthday,
            'email' => $data['email'],
            'password' => $pwd,
            'reminder' => bin2hex(openssl_random_pseudo_bytes(16)),
            'permissions' => [
                'user.delete' => 0,
            ],
        ]);

        $activation = (new IlluminateActivationRepository())->create($user);
        $mail_contains = "Click on the link below : \n\n" . $this->request->getUri()->getBaseUrl() . '/auth/activate/' . $activation->code . '/' . $id;
        $this->sendEmail("Activate your account sportnet (no reply)", $mail_contains, $user->email);
        $this->get('flash')->addMessage('success', 'Veuillez consulter votre mail pour activer votre compte.');

        return $this->redirect('/');
    }

    /*
     * Methode permettant d'authentifier un utilisateur et de creer la session correspondante
     */
    public function login()
    {
        $data = $this->getPost();
        $remember = isset($data['remember']) && $data['remember'] == 'on' ? true : false;

        $pwd = sha1(self::PREFIX_SALT . $data['password'] . self::SUFFIX_SALT);
        if (!$this->get('sentinel')->authenticate([
            'login' => $data['email'],
            'password' => $pwd,
        ], $remember)
        ) {
            $this->get('flash')->addMessage('error', 'Email ou mot de passe incorrect.');
            return $this->redirect('/auth/login');
        } else {
            $this->get('flash')->addMessage('success', "Vous vous êtes connecté avec succès, bienvenue !");
            return $this->redirect('/');
        }
    }

    /**
     * Methode envoyant un email a l'utilisateur
     */
    public function sendEmail($sujet, $contains, $mail_recipient)
    {
        // Create Transport
        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 587, "tls")
            ->setUsername('kebabgram@gmail.com')
            ->setPassword('tototutu');

        // Create Mailer with our Transport.
        $mailer = \Swift_Mailer::newInstance($transport);
        // Here I'm fetching my email template from my template directory.
        $welcomeEmail = $this->get('view')->fetch('email/email.html', [
            'contains' => $contains
        ]);

        // Setting all needed info and passing in my email template.
        $message = \Swift_Message::newInstance($sujet)
            ->setFrom(array('kebabgram@gmail.com' => 'Sportnet'))
            ->setTo(array($mail_recipient => 'You'))
            ->setBody($welcomeEmail)
            ->setContentType("text/html");
        // Send the message
        $results = $mailer->send($message);

        // Print the results, 1 = message sent!
        print($results);
    }

    /*
     * It's function to confirm account (change value in table activation)
     * Sentinel contains already method for that
     */
    public function confirm_account($code, $login)
    {
        //Sentinel object
        $activation = new IlluminateActivationRepository();

        //user not exist
        if (!($user = $this->get('sentinel')->findById($login))) {
            $this->get('flash')->addMessage('error', 'Aucune adresse email ne correspond à cet utilisateur.');
            return $this->redirect('/');
        }
        if (!$activation->complete($user, $code)) {
            if ($activation->completed($user)) {
                $this->get('flash')->addMessage('success', 'Votre compte a déjà été activé, il vous suffit de vous connecter !');
                return $this->redirect('/auth/login');
            }
            $this->get('flash')->addMessage('error', "Une erreur est survenue lors de l'activation du compte.");
            return $this->redirect('/');
        }

        $user = Sentinel::findById($login);
        Sentinel::login($user);
        $this->get('flash')->addMessage('success', 'Votre compte a bien été activé, bienvenue !');

        return $this->redirect('/auth/login');
    }


    /*
     * Function to logout user
     */
    public function logout()
    {
        $this->get('sentinel')->logout();
        $this->get('flash')->addMessage('success', 'Vous vous êtes bien déconnecté. À bientôt !');
        return $this->redirect('/');
    }

    /*
     * Function to return first view to reminder password
     */
    public function reminderPwd()
    {
        $this->render('auth/reminder.html');
    }

    /*
     * Send Email which contains link for reset form
     */
    public function reminderPost()
    {
        $data = $this->getPost();
        $user = Sentinel::findByCredentials(['login' => $data['email']]);

        if ($user) {

            $mail_contains = "Click on the link below to change your password : \n\n" . $this->request->getUri()->getBaseUrl() . '/auth/reminder/' . $user->reminder . '/' . $user->id;
            $this->sendEmail("Forgot password  sportnet (no reply)", $mail_contains, $user->email);
            $this->get('flash')->addMessage('success', 'Veuillez consulter vos emails afin de redéfinir votre mot de passe.');
            return $this->redirect('/auth/reminder');
        } else {
            $this->get('flash')->addMessage('error', 'Cette adresse email ne correspond à aucun compte.');
            return $this->redirect('/auth/reminder');
        }
    }

    /*
    * return form for change password
    */
    public function reminderForm($token, $user_id)
    {
        $user = EloquentUser::where('id', '=', $user_id)->where('reminder', '=', $token)->first();

        if (!$user) {
            $this->get('flash')->addMessage('error', 'Le lien est arrivé à expiration, veuillez saisir votre adresse email à nouveau pour continuer.');
            return $this->redirect('/auth/reminder');
        }
        return $this->render('auth/reminderForm.html', [
            'user_reminder' => $user
        ]);
    }

    /*
     * Submit form to change password
     */
    public function reminderChangePwd($token, $user_id)
    {
        $data = $this->getPost();
        $user = EloquentUser::where('id', '=', $user_id)->where('reminder', '=', $token)->first();

        if ($user) {

            if ($data['password'] != $data['confirm_password']) {
                $this->get('flash')->addMessage('error', 'Le mot passe et la confirmation du mot de passe ne correspondent pas.');
                return $this->redirect('/reminder');
            }

            $pwd = sha1(self::PREFIX_SALT . $data['password'] . self::SUFFIX_SALT);
            $credentials = [
                'email' => $user->email,
                'password' => $pwd,
                'reminder' => bin2hex(openssl_random_pseudo_bytes(16))
            ];
            Sentinel::update($user, $credentials);
            $this->get('flash')->addMessage('success', 'Votre mot de passe a bien été modifié.');
            return $this->redirect('/');
        } else {
            $this->get('flash')->addMessage("error', 'Un erreur s'est produite lors du changement de mot de passe. Veuillez réessayer !");
            return $this->redirect('/auth/reminder');
        }
    }

}