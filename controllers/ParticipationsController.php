<?php

namespace App\Controllers;


use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Model\Participations;
use MartynBiz\Slim3Controller\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Eloquent;

class ParticipationsController extends Controller
{
    public function index($session){
        $participations=array();

        foreach ($session->ChildParticipations->take(10) as $p){
            $p->ParentVisitor;
            $p->ParentUser;
            array_push($participations,$p);
        }

        return(json_encode($participations));

    }

    public function create(){

    }

    public function destroy($id){

    }

    public function edit($id){

    }

    public function update($id){

    }

    public function show($id){

    }
}