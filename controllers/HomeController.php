<?php

namespace App\Controllers;

use App\Model\Sports;
use App\Model\Visitors;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use \MartynBiz\Slim3Controller\Controller;

class HomeController extends Controller {

    protected  $userController;

    public function __construct(\Slim\App $app)
    {
        parent::__construct($app);
        $this->userController = new UserController($app);
        $this->eventController = new EventsController($app);
    }

    /**
     * Fonction qui affiche l'accueil
     */
    public function accueil()
    {
        $events = json_decode($this->eventController->index());
        $sport = json_decode(Sports::all());

        $datas = array('user' => $this->get('sentinel')->check(), 'events' =>$events , 'sports' => $sport, 'title' => 'Derniers Événements');

        return $this->render('index.html', $datas);
    }

    /**
     * fonction qui affiche l'accueil avec les événements liés au sport dont l'id est donné en paramètre
     */
    public function displaySport($id){
        $sport = Sports::find($id);

        if($sport == null ){
            $this->get('flash')->addMessage('error', 'Le sport que vous recherchez n\existe plus');
            return $this->redirect('/');
        }

        $events = json_decode($this->eventController->sportIndex($id));
        $sports = json_decode(Sports::all());

        $datas = array('user' => $this->get('sentinel')->check(),
                                            'events' =>$events ,
                                            'sports' => $sports,
                                            'title' => 'Derniers Événements : '.$sport->name,
                                            'sport' => $sport
                    );

        return $this->render('index.html', $datas);
    }

}