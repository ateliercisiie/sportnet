<?php


namespace App\Controllers;


use App\Model\Participations;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Model\Sessions;
use MartynBiz\Slim3Controller\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Eloquent;
use App\Utils\CSVUtility;
use App\Utils\UploadUtility;

use App\Model\Events;

class SessionsController extends Controller
{

    /**
     * Methode retournant la vue du formulaire de creation d'un evenement
     */
    public function createSessionView($eventId) {
        $datas = array('user' => $this->get('sentinel')->getUser(), 'eventId' => $eventId);
        
        return $this->render('sessions/create.html', $datas);
    }
    
    /**
     * Methode verifiant la validite d'une epreuve avant l'ajout dans la base
     */
    public function addSession() {
        $data = $this->getPost();

        if(!empty($data['sessionName']) && !empty($data['description']) && !empty($data['hours']) && !empty($data['minutes']) && !empty($data['eventId'])) {
            //Tous les champs ont bien été complétés
            $event = Events::find($data['eventId']); //On récupère l'événement associé à l'épreuve
            
            if($event != null) {
                if($event->user_id == $this->get('sentinel')->getUser()->id) {
                    //L'événement appartient bien à l'utilisateur
                    if($data['hours'] <= 23 && $data['hours'] >= 0) {
                        if($data['minutes'] <= 59 && $data['minutes'] >= 0) {
                            //L'heure est correcte, on concatène la date de l'even et l'heure fournie
                            $date = new \DateTime($event->date);
                            $stringDate = $date->format('Y-m-d');
                            $stringDate .= ' '.$data['hours'].':'.$data['minutes'].':00';

                            $sessionData = array(
                                'id' => Uuid::uuid4(),
                                'name' => $data['sessionName'],
                                'date' => $stringDate,
                                'description' => $data['description'],
                                'results_available' => false,
                                'prix' => 0,
                                'event_id' => $data['eventId']);

                            $this->create($sessionData);

                            $this->get('flash')->addMessage('success', 'Votre épreuve a bien été créée.');
                            return $this->redirect('/events/display/'.$data['eventId']);
                        } else {
                            //La valeur des minutes est incorrect
                            $this->get('flash')->addMessage('error', 'La valeur des minutes doit être comprise en 0 et 59.');
                            return $this->redirect('/sessions/create/'.$data['eventId']);
                        }
                    } else {
                        //La valeur de l'heure est incorrecte
                        $this->get('flash')->addMessage('error', 'La valeur des heures doit être comprise en 0 et 23.');
                        return $this->redirect('/sessions/create/'.$data['eventId']);
                    }
                } else {
                    //L'événement n'appartient pas à l'utilisateur qui a rempli le champ
                    $this->get('flash')->addMessage('error', 'L\'événement auquel vous tentez d\'ajouter une épreuve ne vous appartient pas.');
                    return $this->redirect('/sessions/create/'.$data['eventId']);
                }   
            } else {
                $this->get('flash')->addMessage('error', 'Aucun événement n\'est associé à votre épreuve. Veuillez créer un événement avant de créer une épreuve.');
                return $this->redirect('/events/create');
            }
        } else {
            //Aucun champ ne doit être laissé vide
            $this->get('flash')->addMessage('error', 'Tous les champs doivent être complétés pour créer une épreuve.');
            return $this->redirect('/sessions/create/'.$data['eventId']);
        }
    }
    
    /**
     * Methode permettant d'afficher une epreuve
     * @param id de l'epreuve (method GET)
     */
    public function displaySession($sessionId) {
        $session = Sessions::find($sessionId);

        //Si l'epreuve n'existe pas
        if($session == null) {
            $this->get('flash')->addMessage('error', 'L\'épreuve que vous recherchez n\'existe pas ou plus.');
            return $this->redirect('/');
        }

        $participations = $session->ChildParticipations()->where('already_payed', 1)->get();
        $participants = array();
        foreach ($participations as $v){
            $participants[] = $v->ParentVisitor;
        }

        $data = ['sessionObject' => $session,'participations' => $participants,'user' => $this->get('sentinel')->getUser()];

        //Si les resultats sont disponibles, on les inclut a la vue
        if($session->results_available) {
            $csvConverter = new CSVUtility();
            $csvArrayResults = $csvConverter->readCSV('csv/results-'.$sessionId.'.csv');

            $data['results'] = $csvArrayResults; 
        }
        
        return $this->render('sessions/session.html', $data);
    }
    
    /**
     * Methode faisant les verifications necessaires avant de supprimer une epreuve
     */
    public function removeSession($id) {
        $session = Sessions::find($id);
        
        //Si l'epreuve n'existe pas
        if($session == null) {
            $this->get('flash')->addMessage('error', 'L\'épreuve que vous essayez de supprimer n\'existe pas.');
            return $this->redirect('/');
        }
        
        //Si l'epreuve n'appartient pas a l'utilisateur
        if($session->ParentEvent->user_id != $this->get('sentinel')->getUser()->id) {
            $this->get('flash')->addMessage('error', 'L\'épreuve que vous essayez de supprimer ne vous appartient pas.');
            return $this->redirect('/sessions/display/'.$id);
        }
        
        $this->destroy($id);
        
        $this->get('flash')->addMessage('success', 'L\'épreuve a bien été supprimée.');
        return $this->redirect('/events/display/'.$session->event_id);
    }
    
    /**
     * Methode permettant de generer un fichier CSV contenant la liste des participants
     * @param $sessionId - id de l'epreuve
     * @param $header - inclure l'en tete ('numero de dossard, prenom, nom') au fichier CSV (true par defaut)
     */
    public function getParticipantsFile($sessionId) {
        $session = Sessions::find($sessionId);
        
        if($session != null) {
            //L'epreuve existe
            if($session->parentEvent->user_id == $this->get('sentinel')->getUser()->id) {
                //L'épreuve appartient bien à l'utilsateur
                $participants = $session->ChildParticipations()->where('already_payed', 1)->get();
                $numeroDossard = 1;

                $csvParticipantsArray = array();

                foreach($participants as $participant) {
                    $user = $participant->ParentVisitor;
                    $csvParticipantsArray[] = array($numeroDossard, $user->first_name, $user->last_name); //On ajoute la ligne au tableau

                    $numeroDossard++;
                }

                $header = array('Numéro de Dossard', 'Prénom', 'Nom');
                $generator = new CSVUtility($csvParticipantsArray, $header);
                $generator->getCSV('participants.csv'); 
            } else {
                //L'epreuve n'appartient pas a l'utilisateur
                $this->get('flash')->addMessage('error', 'L\'épreuve dont vous souhatez télécharger les participants ne vous appartient pas.');
                return $this->redirect('/');
            }
        } else {
            //L'epreuve n'existe pas
            $this->get('flash')->addMessage('error', 'L\'épreuve dont vous souhaitez télécharger les participants n\'existe pas ou plus.');
            return $this->redirect('/');
        }
    }
    
    /**
     * Methode permettant d'uploader les resultats pour une epreuve
     */
    public function uploadResults($id)
    {
        $session = Sessions::find($id);

        //Si l'epreuve n'existe pas
        if ($session == null) {
            $this->get('flash')->addMessage('error', 'L\'épreuve dont vous souhaitez envoyer les résultats n\'existe pas ou plus.');
            return $this->redirect('/');
        }

        //Si l'epreuve n'appartient pas a l'utilsiateur
        if ($session->ParentEvent->user_id != $this->get('sentinel')->getUser()->id) {
            $this->get('flash')->addMessage('error', 'L\'épreuve dont vous souhaitez envoyer les résultats ne vous appartient pas.');
            return $this->redirect('/sessions/display/'.$id);
        }

        //Si les resultat ont deja ete publies
        if ($session->results_available) {
            $this->get('flash')->addMessage('error', 'Les résultats de l\'épreuve ont déjà été publiés.');
            return $this->redirect('/sessions/display/'.$id);
        }
        
        if ($_FILES['resultsFile']['size'] == 0) {
            $this->get('flash')->addMessage('error', 'Vous devez sélectionner un fichier pour publier les résultats.');
            return $this->redirect('/sessions/display/'.$id);            
        }

        $validations = array(
            'nameFile' => 'results-' . $id,
            'validations' => ['text/csv', 'text/plain']
        );
        
        $uploadUtility = new UploadUtility('csv/', 'resultsFile', $validations);
        $uploadResults = $uploadUtility->upload();

        //Si l'upload a echoue
        if (!$uploadResults['success']) {
            $this->get('flash')->addMessage('error', $uploadResults['error']);
            return $this->redirect('/sessions/display/'.$id);
        }

            $csvConverter = new CSVUtility();
            $csvConverterResults = $csvConverter->readCSV('csv/' . $uploadResults['name']);

            //Si la conversion a echoue
            if (!$csvConverterResults) {
                $this->get('flash')->addMessage('error', 'Une erreur est survenue lors de la conversion du fichier en CSV. Veuillez vérifier qu\'il est correctement formaté !');
                return $this->redirect('/sessions/display/'.$id);
            }

            //On met a jour la base
            $session->results_available = true;
            $session->save();

            $this->get('flash')->addMessage('success', 'Les résultats de l\'épreuve ont bien été publiés.');
            return $this->redirect('/sessions/display/'.$id);

    }
    /**
     * Methode permettant de supprimer la publication des resultats d'une epreuve
     */
    public function hideResults($id) {
        $session = Sessions::find($id);
        
        if($session == null) {
            $this->get('flash')->addMessage('error', 'L\'épreuve n\'existe pas ou plus.');
            return $this->redirect('/');   
        }
        
        if($session->ParentEvent->user_id != $this->get('sentinel')->getUser()->id) {
            $this->get('flash')->addMessage('error', 'L\'épreuve que vous tentez de modifier ne vous appartient pas.');
            return $this->redirect('/');    
        }
        
        $session->results_available = 0;
        $session->save();
        
        $this->get('flash')->addMessage('success', 'Les résultats de l\'épreuve ont bien été retirés de la publication.');
        return $this->redirect('/sessions/display/'.$id);
    }
    
    public function index(){

    }

    /**
     * Methode ajoutant une epreuve dans la base de donnees
     * @param $data: donnees a ajouter a la BD
     * @return id de l'epreuve cree
     */
    public function create($data){
        $session = new Sessions();
        $session->id = $data['id'];
        $session->name = $data['name'];
        $session->date = $data['date'];
        $session->event_id = $data['event_id'];
        $session->results_available = $data['results_available'];
        $session->prix = $data['prix'];
        $session->description = $data['description'];
        
        $session->save();
    }

    /**
     * Methode permettant de supprimer une epreuve dans la base de donnees
     * @param $id - id de l'epreuve a supprimer
     */
    public function destroy($id){
        $session = Sessions::find($id);
        $session->delete();
    }

    public function edit($id){

    }

    public function update($id){

    }

    public function show($id){

    }
}