<?php

namespace App\Controllers;


use App\Model\Participations;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Model\Visitors;
use App\Model\Sessions;
use MartynBiz\Slim3Controller\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Eloquent;


class VisitorsController extends Controller
{

    public function index()
    {

    }

    public function createJoinView($id){
        $user = $this->get('sentinel')->check();
        return $this->render('visitors/create.html', ['idSession' => $id, 'user' => $user]);
    }

    /** Methode permettant de s'inscrire a une la session
     * @param $id , de la session
     * @return
     */
    public function join($id)
    {
        $data = $this->getPost();
        $message = '';
        $idSession = $id;
        $array_participation = array();

        $size = sizeof($data) / 5;

        var_dump($data);

        $session = Sessions::find($idSession);
        if (!$session->ParentEvent->registrable) {
            $this->get('flash')->addMessage('error', 'Les inscriptions ne sont plus ouvertes');
            return $this->redirect('/');
        }

        for ($i = 1; $i <= $size; $i++) {
            if (!empty($idSession) && !empty($data['lastname_' . $i])
                && !empty($data['firstname_' . $i]) && !empty($data['jour_' . $i])
                && !empty($data['mois_' . $i]) && !empty($data['annee_' . $i])
            ) {

                $date = new \DateTime($data['annee_' . $i] . '-' . $data['mois_' . $i] . '-' . $data['jour_' . $i]);
                $stringDate = $date->format('Y-m-d');

                $visitor = new Visitors();

                $visitor->id = Uuid::uuid4()->toString();
                $visitor->last_name = $data['lastname_' . $i];
                $visitor->first_name = $data['firstname_' . $i];
                $visitor->birthdate = $stringDate;
                $visitor->save();

                $participation = new Participations();
                $participation->id = Uuid::uuid4();
                $participation->visitor_id = $visitor->id;
                $participation->session_id = $idSession;
                $participation->already_payed = 0;
                $participation->save();

                $array_participation[] = $participation;

                $message .= '<br>Numéro participant ' . $i . ': ' . $participation->id;

            } else {
                $this->get('flash')->addMessage('error', 'Veuillez remplir tous les champs');
                return $this->redirect('/visitors/join/' . $idSession);
            }
        }

        $_SESSION['array_participation'] = $array_participation;

        $this->get('flash')->addMessage('success', 'Vos participations ont bien été enregistrées Veuillez noter vos/votre numéro(s) de participant :' . $message);
        return $this->redirect('/visitors/pay/' . $idSession);
    }

    /** Methode qui gère le paiement
     * @param $id , de la session
     * @return Controller
     */
    public function pay($id)
    {

        if (isset($_SESSION['array_participation'])) {

            $session = Sessions::find($id);

            if ($session != null) {
                return $this->render('visitors/pay.html', [
                    'participations' => $_SESSION['array_participation'],
                    'sessions' => $session
                ]);

            }

        } else {
            $this->get('flash')->addMessage('error', "Aucune participation n'a été enregistrée");
            return $this->redirect('/');
        }

    }

    public function payPost($id)
    {

        if (!isset($_SESSION['array_participation'])) {
            $this->get('flash')->addMessage('error', 'Aucune participation a été enregisté');
            return $this->redirect('/');
        }
        foreach ($_SESSION['array_participation'] as $participation) {
            $participation->already_payed = 1;
            $participation->save();
        }
        unset($_SESSION['array_participation']);

        $this->get('flash')->addMessage('success', 'Votre partication et votre paiement ont bien été pris en compte');
        return $this->redirect('/sessions/display/' . $id);
    }

    public function create()
    {

    }

    public function destroy($id)
    {

    }

    public function edit($id)
    {

    }

    public function update($id)
    {

    }

    public function show($id)
    {

    }


}