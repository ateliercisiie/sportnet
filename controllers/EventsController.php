<?php

namespace App\Controllers;


use App\Model\Participations;
use App\Utils\UploadUtility;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Model\Events;
use MartynBiz\Slim3Controller\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Eloquent;

use App\Model\Sports;


class EventsController extends Controller
{

    public function index(){

        $events  = Events::where('validated', true)->orderBy("created_at", 'desc')->take(4)->get(array('id', 'name', 'location','date', 'registrable','user_id', 'description', 'sport_id'));

        foreach ($events as $v){
            $v->sport;
            $v->parentOwner;
        }

        return json_encode($events);
    }

    public function sportIndex($id){

        $events  = Events::where('sport_id',$id)->orderBy("created_at", 'desc')->take(4)->get(array('id', 'name', 'location','date', 'registrable','user_id', 'description', 'sport_id'));

        foreach ($events as $v){
            $v->sport;
            $v->parentOwner;
        }

        return json_encode($events);
    }


    /**
     * Methode retournant la vue du formulaire de creation d'un evenement
     */
    public function createEventView()
    {
        $datas = array(
            'sports' => Sports::all(),
            'user' => $this->get('sentinel')->getUser());
        return $this->render('events/create.html', $datas);
    }

    /**
     * Methode verifiant la validite d'un evenement avant l'ajout dans la base
     */
    public function addEvent()
    {
        $data = $this->getPost();

        //Vérifie si les cases à cocher ont été sélectionnées
        $data['validated'] = isset($data['validated']);
        $data['registrable'] = isset($data['registrable']);

        //Récupère le fichier image
        $file = $_FILES;

        if (!empty($data['eventName']) && !empty($data['eventDate']) && !empty($data['location']) && !empty($data['description']) && !empty($data['sport']) && !empty($file['picture']['name'])) {
            //Tous les champs ont bien été complétés

            $date = new \DateTime($data['eventDate']);
            $eventDate = $date->format('Y-m-d H:i:s');

            $eventData = array(
                'id' => Uuid::uuid4(),
                'name' => $data['eventName'],
                'date' => $eventDate,
                'location' => $data['location'],
                'description' => $data['description'],
                'registrable' => $data['registrable'],
                'validated' => $data['validated'],
                'sport_id' => $data['sport'],
                'user_id' => $this->get('sentinel')->getUser()->id);

            $path = 'images/events/';
            $fileUpload = new UploadUtility($path, 'picture', ['nameFile' => $eventData['id']]);
            $success = $fileUpload->upload();
            if (!$success['success']) {
                $this->get('flash')->addMessage('error', $success['error'][0]);
                return $this->redirect('/events/create');
            }

            $this->create($eventData);

            $this->get('flash')->addMessage('success', 'Votre événement a bien été créé.');
            return $this->redirect('/events/display/' . $eventData['id']);
        } else {
            //Aucun champ ne doit être laissé vide
            $this->get('flash')->addMessage('error', 'Tous les champs doivent être complétés pour créer un événement.');
            return $this->redirect('/events/create');
        }
    }
    
    /**
     * Methode permettant de publier ou depublier un evenement
     */
    public function updatePublication($id) {
        $event = Events::find($id);
        
        //Si l'evenement n'existe pas
        if($event == null) {
            $this->get('flash')->addMessage('error', 'L\'événement que vous tentez de modifier n\'existe pas.');
            return $this->redirect('/');    
        }
        
        //Si l'evenement n'appartient pas a l'utilsateur
        if($event->user_id != $this->get('sentinel')->getUser()->id) {
            $this->get('flash')->addMessage('error', 'L\'événement que vous tentez de modifier ne vous appartient pas.');
            return $this->redirect('/events/display/'.$id);    
        }
        
        $event->validated = !$event->validated;
        $event->save();
        
        if($event->validated) {
            $this->get('flash')->addMessage('success', 'L\'événement a bien été publié.');
            return $this->redirect('/events/display/'.$id);    
        } else {
            $this->get('flash')->addMessage('success', 'L\'événement a bien été dépublié.');
            return $this->redirect('/events/display/'.$id);            
        } 
    }

    /**
     * Methode permettant d'afficher un evenement a partir de son id
     * @param evenId : id de l'event (method GET)
     */
    public function displayEvent($eventId)
    {
        $event = Events::find($eventId); //On récupère l'événement dans la BD

        //Si l'event n'a pas été trouve
        if ($event == null) {
            $this->get('flash')->addMessage('error', 'L\'événement que vous souhaitez afficher n\'existe pas ou plus.');
            return $this->redirect('/');
        }
        
        //Si l'event n'est pas publie
        if(!$event->validated && ($event->user_id != $this->get('sentinel')->getUser()->id || !$this->get('sentinel')->check())) {
            $this->get('flash')->addMessage('error', 'L\'événement que vous souhaitez afficher n\'est pas publié.');
            return $this->redirect('/');           
        }


        $sessions = $event->ChildSessions;
        $currentUserId = 0;
        $countTab = array();

        if (Sentinel::check()) {
            $currentUserId = Sentinel::getUser()->getUserId();
        } else {
            if (!empty($_SESSION['visitor_id'])) {
                $currentUserId = $_SESSION['visitor_id'];
            }
        }

        if ($currentUserId != 0) {
            foreach ($sessions as $session) {

                $count = 0;
                foreach ($session->ChildParticipations as $c) {
                    $count++;
                    if ($c->user_id == Sentinel::getUser()->getUserId()) {
                        $session->already_joined = true;
                    }
                }
                $countTab[$session->id] = $count;
            }
        } else {
            foreach ($sessions as $session) {
                $count = 0;
                foreach ($session->ChildParticipations as $c)
                    $count++;
                $countTab[$session->id] = $count;
            }
        }

        return $this->render('events/event.html',
            ['event' => $event,
                'sessions' => $sessions,
                'user' => $this->get('sentinel')->getUser(),
                'session_php' => $_SESSION,
                'countTab' => $countTab
            ]);
    }

    /**
     * Methode faisant les verifications necessaires avant de supprimer un evenement
     */
    public function removeEvent($id) {
        $event = Events::find($id);
        
        //Si l'evenement n'existe pas
        if($event == null) {
            $this->get('flash')->addMessage('error', 'L\'événement que vous essayez de supprimer n\'existe pas.');
            return $this->redirect('/');
        }
        
        //Si l'evenement n'appartient pas a l'utilisateur
        if($event->user_id != $this->get('sentinel')->getUser()->id) {
            $this->get('flash')->addMessage('error', 'L\'événement que vous essayez de supprimer ne vous appartient pas.');
            return $this->redirect('/events/display/'.$id);
        }
        
        $this->destroy($id);
        
        $this->get('flash')->addMessage('success', 'L\'événement a bien été supprimé.');
        return $this->redirect('/');
    }



    /**
     * Methode ajoutant un evenement dans la base de donnees
     * @param $data: donnees a ajouter a la BD
     * @return id de l'event cree
     */
    public function create($data){
        $event = new Events();
        $event->id = $data['id'];
        $event->name = $data['name'];
        $event->date = $data['date'];
        $event->location = $data['location'];
        $event->description = $data['description'];
        $event->registrable = $data['registrable'];
        $event->validated = $data['validated'];
        $event->sport_id = $data['sport_id'];
        $event->user_id = $data['user_id'];

        $event->save();

        return $event->id;
    }

    /**
     * Methode permettant de supprimer un evenement dans la base de donnees
     * @param $id - id de l'event a supprimer
     */
    public function destroy($id){
        $event = Events::find($id);
        $event->delete();
    }

    public function edit($id){

    }

    public function update($id){

    }

    public function show($id){

    }

    public function getMore($id){
        $event = Events::where('id', '=', $id)->first();
        $events = Events::orderby('created_at', 'desc')
            ->where("created_at", '<', $event->created_at)
            ->where("validated", true)
            ->take(4)
            ->get();

        foreach ($events as $v){
            $v->sport;
            $v->parentOwner;
        }

        return json_encode($events);
    }

    public function getMoreSport($idEvent, $idSport){
        $event = Events::where('id', '=', $idEvent)->first();
        $events = Events::orderby('created_at', 'desc')
            ->where('sport_id', $idSport)
            ->where("created_at", '<', $event->created_at)
            ->take(4)
            ->get();

        foreach ($events as $v){
            $v->sport;
            $v->parentOwner;
        }

        return json_encode($events);
    }
    public function getMoreByUser($id_event, $id_user){
        $event = Events::where('id', '=', $id_event)->first();
        $events = Events::orderby('created_at', 'desc')
            ->where('user_id', $id_user)
            ->where("created_at", '<', $event->created_at)
            ->take(4)
            ->get();

        foreach ($events as $v){
            $v->sport;
            $v->parentOwner;
        }

        return json_encode($events);
    }

    public function closeInscription($id){
        $event = Events::find($id);

        //si l'evenement n'existe pas
        if($event == null){
            $this->get('flash')->addMessage('error', 'L\'événement que vous essayez de modifier n\'existe pas.');
            return $this->redirect('/');
        } else {
            $event->registrable = false;
            $event->update();
            $this->get('flash')->addMessage('success', 'Les inscriptions sont fermées');
            return $this->redirect('/events/display/'.$id);
        }
    }

    public function openInscription($id){
        $event = Events::find($id);

        //si l'evenement n'existe pas
        if($event == null){
            $this->get('flash')->addMessage('error', 'L\'événement que vous essayez de modifier n\'existe pas.');
            return $this->redirect('/');
        } else {
            $event->registrable = true;
            $event->update();
            $this->get('flash')->addMessage('success', 'Les inscriptions sont ouvertes');
            return $this->redirect('/events/display/'.$id);
        }
    }
}