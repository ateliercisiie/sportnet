<?php

namespace App\Model;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\Model;
class Comments extends Model
{
    protected $table = 'comments';
    public $timestamps = true;
    protected $primaryKey= 'id';
    public $incrementing = false;
    protected $fillable = ['id','user_id','event_id','text'];

    public function ParentWriter(){
        return $this->belongsTo('Cartalyst\Sentinel\Users\EloquentUser', 'user_id' ) ;
    }

    public function ParentEvent(){
        return $this->belongsTo('App\Model\Sessions', 'event_id' ) ;
    }


}
