<?php

namespace App\Model;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\Model;
class Participations extends Model
{
    protected $table = 'participations';
    public $timestamps = true;
    protected $primaryKey= 'id';
    public $incrementing = false;
    protected $fillable = ['id','user_id','visitor_id','session_id','already_played','rank','opinion'];


    public function ParentSession(){
        return $this->belongsTo('App\Model\Sessions', 'session_id' ) ;
    }

    public function ParentVisitor(){
        return $this->belongsTo('App\Model\Visitors', 'visitor_id' );
    }

    public function ParentUser(){
        return $this->belongsTo('Cartalyst\Sentinel\Users\EloquentUser', 'user_id' );
    }


}
