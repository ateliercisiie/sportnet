<?php

namespace App\Model;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\Model;
class Events extends Model
{
    protected $table = 'events';
    public $timestamps = true;
    protected $primaryKey= 'id';
    public $incrementing = false;
    protected $fillable = ['id','name','date','location','description','registrable','hidden','validated','sport_id','user_id'];

    public function ParentOwner(){
        return $this->belongsTo('Cartalyst\Sentinel\Users\EloquentUser', 'user_id' ) ;
    }

    public function ChildSessions(){
        return $this->hasMany('App\Model\Sessions', 'event_id' );
    }

    public function Sport(){
        return $this->belongsTo('App\Model\Sports', 'sport_id' );
    }

}