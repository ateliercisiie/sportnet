<?php

namespace App\Model;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\Model;
class Sessions extends Model
{
    protected $table = 'sessions';
    public $timestamps = false;
    protected $primaryKey= 'id';
    public $incrementing = false;
    protected $fillable = ['id','name','event_id','date','results_available','prix','description'];
    public $already_joined = false;


    public function ParentEvent(){
        return $this->belongsTo('App\Model\Events', 'event_id' ) ;
    }

    public function ChildParticipations(){
        return $this->hasMany('App\Model\Participations', 'session_id' );
    }


}