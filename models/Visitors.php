<?php

namespace App\Model;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\Model;
class Visitors extends Model
{
    protected $table = 'visitors';
    public $timestamps = true;
    protected $primaryKey= 'id';
    public $incrementing = false;
    protected $fillable = ['id','first_name','last_name','birthdate'];

    public function ChildParticipations(){
        return $this->hasMany('App\Model\Participations', 'visitor_id' );
    }

}
