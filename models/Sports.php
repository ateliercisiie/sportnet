<?php

namespace App\Model;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\Model;
class Sports extends Model
{
    protected $table = 'sports';
    public $timestamps = false;
    protected $primaryKey= 'id';
    public $incrementing = false;
    protected $fillable = ['id','name'];


    public function ChildEvents(){
        return $this->hasMany('App\Model\Events', 'sport_id' );
    }


}