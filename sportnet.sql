-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 19 Novembre 2016 à 08:25
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sportnet`
--

-- --------------------------------------------------------

--
-- Structure de la table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(8, 'd94de8d0-c044-4eb6-844c-11e533dee331', '6H5Kq4nW6Owhs9SdBzPH1Qrp0AygECpw', 1, '2016-11-18 14:54:00', '2016-11-18 14:53:46', '2016-11-18 14:54:00'),
(9, 'd272c168-cff5-4d49-a95e-7ed4fcf7493c', 'Mt40GVnl82Jyp69gvgvQqoFYSSaTV3Lx', 1, '2016-11-19 04:25:40', '2016-11-19 04:25:28', '2016-11-19 04:25:40'),
(10, 'ccb2f3ca-2283-4fbe-a533-f47d81b4327b', 'D8Kd6PTw88bIypfzSYzRmIXxnJwlAR4K', 1, '2016-11-19 07:23:45', '2016-11-19 07:23:32', '2016-11-19 07:23:45');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `event_id` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

CREATE TABLE `events` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `location` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `registrable` tinyint(1) NOT NULL DEFAULT '1',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `validated` tinyint(1) NOT NULL DEFAULT '0',
  `sport_id` int(3) NOT NULL DEFAULT '0',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='1 -> open\r\n0 -> close\r\n';

--
-- Contenu de la table `events`
--

INSERT INTO `events` (`id`, `name`, `date`, `location`, `description`, `created_at`, `updated_at`, `registrable`, `hidden`, `validated`, `sport_id`, `user_id`) VALUES
('306f6052-bc75-4d71-b6cf-cde913a9eb53', 'Semi-Marathon de la Saint Nicolas', '2016-12-05 00:00:00', 'Nancy', 'Le semi-marathon de la Saint Nicolas est un grand événement sportif organisé chaque année par la ville de Nancy.\r\n', '2016-11-19 05:38:24', '2016-11-19 05:38:24', 0, 0, 1, 1, 'd272c168-cff5-4d49-a95e-7ed4fcf7493c'),
('535d9742-a909-4433-8875-83cba7c5c022', 'Coupe Universitaire de Volley', '2017-02-08 00:00:00', 'Vandoeuvre-lès-Nancy', 'La coupe de l\'Université de Lorraine de Volleyball est réservée aux étudiants de l\'Université.', '2016-11-19 05:53:04', '2016-11-19 05:53:04', 1, 0, 1, 19, 'd94de8d0-c044-4eb6-844c-11e533dee331'),
('891107c4-4da4-4306-8868-82e1e1d8a613', 'Compétition de Kayak', '2016-11-29 00:00:00', 'Épinal', 'Comme chaque année, la ville d\'Épinal organise sa compétition de Canoé-Kayak. Venez nombreux !', '2016-11-19 05:45:53', '2016-11-19 05:45:53', 1, 0, 1, 10, 'd94de8d0-c044-4eb6-844c-11e533dee331'),
('bbd9a781-4c3c-4c59-ba68-07c87fe6fe09', 'Championnat d\'Escrime de Lorraine', '2017-03-09 00:00:00', 'Metz', 'Le championnat d\'escrime se déroule à Metz annuellement.', '2016-11-19 05:56:14', '2016-11-19 05:56:14', 1, 0, 1, 11, 'd272c168-cff5-4d49-a95e-7ed4fcf7493c'),
('ed509e2d-957a-45ae-8d4f-742d1f84e4ea', 'Coupe du Monde de VTT', '2016-11-15 00:00:00', 'La Bresse', 'La Coupe du Monde de VTT est une grande compétition internationale dont l\'une des étapes se déroule à la Bresse, dans les Vosges.\r\n', '2016-11-19 05:34:27', '2016-11-19 05:34:27', 1, 0, 1, 2, 'd272c168-cff5-4d49-a95e-7ed4fcf7493c');

-- --------------------------------------------------------

--
-- Structure de la table `participations`
--

CREATE TABLE `participations` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `visitor_id` varchar(255) DEFAULT '1',
  `session_id` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `already_payed` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(10) DEFAULT NULL,
  `opinion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='1 -> vistor\r\n2 -> registered user';

-- --------------------------------------------------------

--
-- Structure de la table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(28, 'd94de8d0-c044-4eb6-844c-11e533dee331', 'hwLX4x61AFImf9MdXUKNsNeVWagTZ431', '2016-11-18 14:54:00', '2016-11-18 14:54:00'),
(29, 'd94de8d0-c044-4eb6-844c-11e533dee331', 'f1kIbkOVH2J8qhH4DbVEeBMyK4A22PGy', '2016-11-18 14:54:09', '2016-11-18 14:54:09'),
(33, 'd94de8d0-c044-4eb6-844c-11e533dee331', 'LHjyN0c19GMv41BhfIk7VxL8ubbDmSI9', '2016-11-19 04:22:56', '2016-11-19 04:22:56'),
(35, 'd272c168-cff5-4d49-a95e-7ed4fcf7493c', 'QSuItg8psZVKY5v8vS0v5n6kBI0khlCy', '2016-11-19 04:25:52', '2016-11-19 04:25:52'),
(36, 'd94de8d0-c044-4eb6-844c-11e533dee331', 'P7qJY04xmwOG2dNyCraKDt098lmOyHez', '2016-11-19 04:42:57', '2016-11-19 04:42:57'),
(37, 'ccb2f3ca-2283-4fbe-a533-f47d81b4327b', 'QlODyLd5STNnNdTcDzCfff3e76BEgVZx', '2016-11-19 07:23:45', '2016-11-19 07:23:45'),
(38, 'ccb2f3ca-2283-4fbe-a533-f47d81b4327b', 'A9UBaYUdMWXsct6GNMz78DxFcYrpkA3N', '2016-11-19 07:23:48', '2016-11-19 07:23:48');

-- --------------------------------------------------------

--
-- Structure de la table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `results_available` tinyint(1) DEFAULT NULL,
  `prix` int(3) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sessions`
--

INSERT INTO `sessions` (`id`, `name`, `date`, `event_id`, `results_available`, `prix`, `description`) VALUES
('46e5999d-a336-406e-b935-1242aca1165f', 'Championnat Féminin', '2017-03-09 13:30:00', 'bbd9a781-4c3c-4c59-ba68-07c87fe6fe09', 0, 0, 'La partie Féminine du Championnat se déroulera à 13h30.'),
('6179d72a-3f98-46fd-82cb-cf6947491731', 'Compétition Handicapés', '2016-12-05 20:10:00', '306f6052-bc75-4d71-b6cf-cde913a9eb53', 0, 0, 'Cette course est réservée aux personnes handicapées.'),
('79b7b674-e1d7-4f97-aa96-76d45e94f273', 'Championnat Enfant', '2016-11-15 10:15:00', 'ed509e2d-957a-45ae-8d4f-742d1f84e4ea', 0, 0, 'Les enfants sont des personnes âgées entre 5 et 12 ans.\r\nL\'épreuve comporte 2 descentes.'),
('b24b1276-6cbb-4126-bfa6-6302f9d6c49b', 'Coupe Féminine', '2017-02-08 09:20:00', '535d9742-a909-4433-8875-83cba7c5c022', 0, 0, 'Compétition féminine de la coupe Universitaire'),
('d142b24c-ff3a-4256-9246-d2dd8a8f7d8a', 'Compétition Mixte', '2016-11-29 14:15:00', '891107c4-4da4-4306-8868-82e1e1d8a613', 0, 0, 'La compétition (mixte) consistera à terminer le parcours en un temps minimum.'),
('d5fe6803-83d2-41ca-91e7-3806b78f0306', 'Coupe Masculine', '2017-02-08 13:25:00', '535d9742-a909-4433-8875-83cba7c5c022', 0, 0, 'Compétition masculine de la coupe Universitaire'),
('d7d66526-96d8-44f7-9db0-042c87ef7bd2', 'Compétition Hommes', '2016-12-05 18:10:00', '306f6052-bc75-4d71-b6cf-cde913a9eb53', 0, 0, 'La course Hommes se déroule 30 minutes avant le départ de la course Femmes.'),
('d970342d-d4b6-4b3b-853a-05c6bda17b66', 'Championnat Masculin', '2017-03-09 14:15:00', 'bbd9a781-4c3c-4c59-ba68-07c87fe6fe09', 0, 0, 'Le championnat masculin se déroulera à 14h15.'),
('e3d3cd72-4a6d-4c63-ba0d-099d17fbe4ae', 'Compétition Enfants', '2016-12-05 12:15:00', '306f6052-bc75-4d71-b6cf-cde913a9eb53', 0, 0, 'La course est destinée aux enfants.'),
('e476f594-ea68-41ce-a283-a2a6d0971b13', 'Compétition Femmes', '2016-12-05 18:40:00', '306f6052-bc75-4d71-b6cf-cde913a9eb53', 0, 0, 'La course femmes se déroule 30 minutes après le départ de la course hommes.'),
('e5ffbd51-43f4-43a5-84ef-f768d375387f', 'Championnat Adulte', '2016-11-15 15:30:00', 'ed509e2d-957a-45ae-8d4f-742d1f84e4ea', 0, 0, 'Les adultes sont des personnes âgées de plus de 18 ans. \r\nL\'épreuve comporte 3 descentes différentes.\r\n'),
('f490370b-7887-4291-ad18-07ffd1e19a18', 'Cours de Kayak - Enfants', '2016-11-29 12:45:00', '891107c4-4da4-4306-8868-82e1e1d8a613', 0, 0, 'Les enfants se verront offrir un cours de Kayak avec un sportif professionnel, médaillé olympique de Sydney.');

-- --------------------------------------------------------

--
-- Structure de la table `sports`
--

CREATE TABLE `sports` (
  `id` int(3) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sports`
--

INSERT INTO `sports` (`id`, `name`) VALUES
(1, 'Course'),
(2, 'Vélo'),
(3, 'Roller'),
(4, 'Aïkido'),
(5, 'Athlétisme'),
(6, 'Aviron'),
(7, 'Badminton'),
(8, 'Basketball'),
(9, 'Boxe'),
(10, 'Canoé-Kayak'),
(11, 'Escrime'),
(12, 'Football'),
(13, 'Gymnastique'),
(14, 'Natation'),
(15, 'Rugby'),
(16, 'Ski'),
(17, 'Tennis de Table'),
(18, 'Tir à l\'Arc'),
(19, 'Volley');

-- --------------------------------------------------------

--
-- Structure de la table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2016-11-17 11:42:29', '2016-11-17 11:42:29'),
(2, NULL, 'ip', '::1', '2016-11-17 11:42:29', '2016-11-17 11:42:29'),
(4, NULL, 'global', NULL, '2016-11-17 12:00:54', '2016-11-17 12:00:54'),
(5, NULL, 'ip', '::1', '2016-11-17 12:00:54', '2016-11-17 12:00:54'),
(7, NULL, 'global', NULL, '2016-11-17 13:47:58', '2016-11-17 13:47:58'),
(8, NULL, 'ip', '::1', '2016-11-17 13:47:58', '2016-11-17 13:47:58'),
(9, NULL, 'global', NULL, '2016-11-17 13:48:27', '2016-11-17 13:48:27'),
(10, NULL, 'ip', '::1', '2016-11-17 13:48:27', '2016-11-17 13:48:27'),
(11, NULL, 'global', NULL, '2016-11-17 13:49:16', '2016-11-17 13:49:16'),
(12, NULL, 'ip', '::1', '2016-11-17 13:49:16', '2016-11-17 13:49:16'),
(13, NULL, 'global', NULL, '2016-11-17 14:21:51', '2016-11-17 14:21:51'),
(14, NULL, 'ip', '::1', '2016-11-17 14:21:51', '2016-11-17 14:21:51'),
(16, NULL, 'global', NULL, '2016-11-18 10:05:52', '2016-11-18 10:05:52'),
(17, NULL, 'ip', '::1', '2016-11-18 10:05:52', '2016-11-18 10:05:52'),
(19, NULL, 'global', NULL, '2016-11-18 10:06:00', '2016-11-18 10:06:00'),
(20, NULL, 'ip', '::1', '2016-11-18 10:06:00', '2016-11-18 10:06:00'),
(22, NULL, 'global', NULL, '2016-11-18 10:06:28', '2016-11-18 10:06:28'),
(23, NULL, 'ip', '::1', '2016-11-18 10:06:28', '2016-11-18 10:06:28'),
(25, NULL, 'global', NULL, '2016-11-19 07:22:13', '2016-11-19 07:22:13'),
(26, NULL, 'ip', '127.0.0.1', '2016-11-19 07:22:13', '2016-11-19 07:22:13'),
(27, NULL, 'global', NULL, '2016-11-19 07:22:15', '2016-11-19 07:22:15'),
(28, NULL, 'ip', '127.0.0.1', '2016-11-19 07:22:15', '2016-11-19 07:22:15');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `reminder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profilePicturePath` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.png',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `reminder`, `last_login`, `first_name`, `last_name`, `birthdate`, `created_at`, `updated_at`, `profilePicturePath`, `description`) VALUES
('ccb2f3ca-2283-4fbe-a533-f47d81b4327b', 'davidlambiase@gmail.com', '$2y$10$jncukrln2vNaR7/N3HczoubZbEw9qsf5TjD/63ykq1IoMQ.x.lMxa', '{"user.delete":0}', 'f15b1ad9a47807831edb8e89c8f83fe1', '2016-11-19 07:23:48', 'David', 'Lambiase', '2016-11-19 08:23:32', '2016-11-19 07:23:32', '2016-11-19 07:23:48', 'default.png', NULL),
('d272c168-cff5-4d49-a95e-7ed4fcf7493c', 'minipapymetal@gmail.com', '$2y$10$L0hwHeYCYIjP4/7Gx32i5e7y0kBtEX4vG2eDE1yX7VRSzO0Ti8Uo2', '{"user.delete":0}', '291e80c98eb7db75a936a0a9488f8c5f', '2016-11-19 04:25:52', 'Rémi', 'Lacroix', '2016-11-19 05:25:28', '2016-11-19 04:25:28', '2016-11-19 04:25:52', 'default.png', ''),
('d94de8d0-c044-4eb6-844c-11e533dee331', 'quentin.claudel@me.com', '$2y$10$AZhbCvfddcCwCmN4/VpF8.oK.nj6ledkm6IrQTaEmuA.rzL.jMzJu', '{"user.delete":0}', '1f5f95cfc2c84bc717b5fff7bba1b28f', '2016-11-19 04:42:57', 'Quentin', 'Claudel', '2015-02-09 00:00:00', '2016-11-18 14:53:46', '2016-11-19 04:42:57', 'd94de8d0-c044-4eb6-844c-11e533dee331.png', 'Je suis un héros !');

-- --------------------------------------------------------

--
-- Structure de la table `visitors`
--

CREATE TABLE `visitors` (
  `id` varchar(255) NOT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id` (`user_id`),
  ADD KEY `comments_event_id` (`event_id`);

--
-- Index pour la table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sport_details` (`sport_id`),
  ADD KEY `events_user_id` (`user_id`);

--
-- Index pour la table `participations`
--
ALTER TABLE `participations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_details` (`session_id`),
  ADD KEY `participations_user_id` (`user_id`),
  ADD KEY `participation_visitor_id` (`visitor_id`);

--
-- Index pour la table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`),
  ADD KEY `persistances_user_id` (`user_id`);

--
-- Index pour la table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reminders_user_id` (`user_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Index pour la table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Index pour la table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_events_id` (`event_id`);

--
-- Index pour la table `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT pour la table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `activations`
--
ALTER TABLE `activations`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_event_id` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sport_details` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `participations`
--
ALTER TABLE `participations`
  ADD CONSTRAINT `participation_visitor_id` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `participations_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sessions_details` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `persistences`
--
ALTER TABLE `persistences`
  ADD CONSTRAINT `persistances_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reminders`
--
ALTER TABLE `reminders`
  ADD CONSTRAINT `reminders_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_events_id` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `throttle_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
