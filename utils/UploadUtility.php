<?php
/**
 * Created by PhpStorm.
 * User: Dylan
 * Date: 17/11/2016
 * Time: 17:00
 */

namespace App\Utils;

/**
 * Classe permettant de gerer l'upload d'un fichier
 * @package App\Utils
 */
class UploadUtility
{
    /**
     * lieu ou enregistrer le fichier
     */
    private $storage;

    /**
     * fichier a enregistrer
     */
    private $file;

    public function __construct($path, $nameInput, array $opt = [])
{

    $this->storage = new \Upload\Storage\FileSystem($path, true);
    $this->file = new \Upload\File($nameInput, $this->storage);

    if(array_key_exists('nameFile', $opt)) 
        $nameFile = $opt['nameFile'];
    else
        $nameFile = uniqid();

    if(array_key_exists('validations', $opt))
        $validations = $opt['validations'];
    else
        $validations = ['image/jpeg'];

    if(array_key_exists('maxSize', $opt))
        $maxSize = $opt['maxSize'];
    else
        $maxSize = '5M';


    $this->file->setName($nameFile);

    $this->file->addValidations(array(
        new \Upload\Validation\Mimetype($validations),

        // Ensure file is no larger than 5M (use "B", "K", M", or "G")
        new \Upload\Validation\Size($maxSize)
    ));

}


    public function upload(){
        try {
            // Success!
            return array('success' => $this->file->upload(), 'name'=>$this->file->getNameWithExtension());
        } catch (\Exception $e) {
            // Fail!
            return array('success'=> false, 'error' => $this->file->getErrors());
        }
    }
}