<?php

namespace App\Utils;

/**
 * Classe permettant de generer un fichier CSV
 */
class CSVUtility {
    /**
     * Donnees a convertir
     */
    private $dataToConvert;
    
    /**
     * En tete du fichier CSV
     */
    private $headerValues;
    
    /**
     * Constructeur de classe
     * @param $dataToConvert - tableau a deux dimensions des donnees a convertir
     * @param $header - en tete du tableau de donnees (null par defaut)
     */
    public function __construct($dataToConvert = null, $header = null) {
        $this->dataToConvert = $dataToConvert;
        $this->headerValues = $header; 
    }
    
    /**
     * Retourne le fichier CSV genere a partir du tableau de donnees
     * @param $filename - Nom du fichier a generer (data.csv par defaut)
     * @param $separator - Separateurs de donnees (virgule par defaut)
     */
    public function getCSV($filename = 'data.csv', $separator = ',') {
        header('Content-Type: text/csv; charset=UTF-8;');
        header('Content-Disposition: attachment; filename='.$filename);
            
        //S'il y a une entete
        if($this->headerValues != null)
            echo implode($separator, $this->headerValues)."\r\n";

        foreach ($this->dataToConvert as $line) {
            echo implode($separator, $line)."\r\n";
        } 
        
        exit;
    }
    
    
    /**
     * Methode permettant de lire un fichier CSV a partir du nom de fichier fourni
     * @param $filePath - Chemin du fichier a lire
     * @param $separator - Separateurs de donnees (virgule par defaut)
     * @return tableau de donnees si la lecture a reussi, faux sinon
     */
    public function readCSV($filePath, $separator = ',') {
        if(($file = fopen($filePath, 'r+')) !== false) {
            //Le fichier a bien été ouvert
            $dataArray = array();
            
            while(($data = fgetcsv($file)) !== false) {
                //Il reste des lignes a lire
                $dataArray[] = $data;
            }
                  
            fclose($file);
                  
            return $dataArray;
        } else return false;
    }
}