<?php
namespace App\middleware;

use Cartalyst\Sentinel\Native\Facades\Sentinel;

class AuthMiddleware
{
    private $container;

    public function __construct($app)
    {
        $this->container = $app->getContainer();
    }

    public function __invoke($request, $response, $next)
    {
        $loggedUser = Sentinel::check();
        if (!$loggedUser) {
            return $response = $response->withStatus(302)->withHeader('location', '/');
        } else {
            return $next($request, $response);
        }
    }
}