<?php
namespace App\middleware;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
class AlreadyLoginMiddleware
{
    private $container;
    public function __construct($app)
    {
        $this->container = $app->getContainer();
    }
    public function __invoke($request, $response, $next)
    {
        $loggedUser = Sentinel::check();
        if ($loggedUser) {
            if($request->getUri()->getPath() == "/auth/login"){
                return $response = $response->withStatus(302)->withHeader('location', '/');
            }
            return $next($request, $response);
        } else {
            return $next($request, $response);
        }
    }
}